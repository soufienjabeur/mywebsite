import { Component, OnInit } from '@angular/core';

export interface Section {
  startingDate: Date;
  endingDate: Date;
  entrepriseName: string;
  postName: string;
  tasks: string[];
  technologiesUsed: string;
  subject?: string;
  link?: string;
}

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  constructor() { }

  experiences: Section[] = [
    {
      startingDate: new Date('09/01/2018'),
      endingDate: new Date('02/28/2019'),
      entrepriseName: 'SWS-TELECOM',
      postName: 'E-commerce manager',
      tasks: ['Manage smartphone prices on the BackMarket platform.',
        'Manage the After-Sales Service (SSA).'],
      technologiesUsed: 'WinGSM, Excel, Google chrome'
    },
    {
      startingDate: new Date('01/01/2018'),
      endingDate: new Date('06/30/2018'),
      entrepriseName: 'GREYC Laboratory, ENSICEAN',
      postName: 'Intern',
      subject: 'Functional encryption to protect the privacy of users of biometric systems',
      tasks: ['Develop a functional encryption scheme.', 'Experiment results.'],
      technologiesUsed: 'Python'
    },
    {
      startingDate: new Date('05/01/2015'),
      endingDate: new Date('08/30/2016'),
      entrepriseName: 'Cluster Technology Company',
      postName: 'Web developer',
      subject: 'Analysis, Design and development of a website for the management of a scientific conference entitled ICACE (The International Conference on Advanced Computer Engineering)',
      tasks: ['Design and development of various login spaces for the administrator, reviewers and authors',
        'Design and development of a space for the submission of scientific papers',
        'Maintenance and bug fixes'],
      technologiesUsed: 'J2EE, Db2, Eclipse IDE, CSS3, HTML5, Bootstrap, Javascript',
      link: 'http://www.webconf.0fees.us/'
    },
    {
      startingDate: new Date('02/01/2015'),
      endingDate: new Date('06/30/2015'),
      entrepriseName: 'ISIM Monastir laboratory',
      postName: 'Intern',
      subject: 'Optimizing Distributed by swarming Particle: Case MaxCSP',
      tasks: ['Develop a solution for the problem under a multi-agent architecture.',
        'Experiment results.'],
      technologiesUsed: 'Java, MySQL, MaDKit 4, Eclipse'
    }

  ];


  ngOnInit(): void {
  }

}
