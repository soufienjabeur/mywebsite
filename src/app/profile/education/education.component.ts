import { Component, OnInit } from '@angular/core';



export interface Section {
  startingDate: string;
  endingDate: string;
  universite: string;
  level: string;
  speciality: string;
  mention?: string;
}
@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  constructor() { }

  education: Section[] = [
    {
      startingDate: '2019',
      endingDate: '2020',
      universite: 'University of Paris- France',
      level: 'Master 2 Professional',
      speciality: 'Information Mobility: Programming, Algorithms for Internet distribution and Systems'
    },
    {
      startingDate: '2017',
      endingDate: '2018',
      universite: 'FS Monastir-Tunisia',
      level: 'Research Master',
      speciality: 'Automatic Reasoning Systems',
      mention: 'Good'
    },
    {
      startingDate: '2015',
      endingDate: '2016',
      universite: 'ISIM Monastir-Tunisia',
      level: 'Master 1 Professional',
      speciality: 'Software engineering',
      mention: 'Good'
    },
    {
      startingDate: '2012',
      endingDate: '2015',
      universite: 'ISIM Monastir-Tunisia',
      level: 'Fundamental licence',
      speciality: 'Computer science',
      mention: 'Good'
    }

  ];



  ngOnInit(): void {
  }

}
