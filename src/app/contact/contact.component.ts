
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';
import { ContactServiceService } from '../services/contact-service.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  formGroup: FormGroup;


  constructor(private formBuilder: FormBuilder, private contactService: ContactServiceService) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.compose([Validators.required, Validators.email])]),
      subject: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required]),
    });
  }



  public sendMail(e: Event) {
    e.preventDefault();
    emailjs.sendForm('service_pvxqcp6', 'template_hf37bya', e.target as HTMLFormElement, 'user_X0ct0UofGs4OkNnf9LUnK')
      .then((result: EmailJSResponseStatus) => {
        console.log(result.text);
      },
        (error) => {
          console.log(error.text);
        });
  }

}
